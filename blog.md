---
layout: page
title: Blog
permalink: /blog/
---

# My Blog

<img style="float:right" src="/images/photo-david-nind.png" alt="Photo of David Nind"/>I live in Wellington, New Zealand and work at Inland Revenue, where I support a small group of colleagues who do [interesting things](http://taxpolicy.ird.govt.nz) (if you like tax, and tax policy).

Some of the things that keep me busy at work include web content management, information management (records management and creating digital archives), and writing documentation.

I'm originally from Oamaru, North Otago (located in New Zealand's wonderful South Island) &ndash; I still consider myself a '[mainlander](https://en.wikipedia.org/wiki/South_Island)'.

I love free and open source software, and good documentation. At home I use an Ubuntu desktop. For servers I use Debian.

At the moment I'm learning to create modular, reusable, topic-based documentation using DITA XML.

I like reading science fiction. 

[INTP](http://www.16personalities.com/intp-personality).

# Contacting me

* Email: david AT davidnind DOT com
* Twitter: [@DavidNind](https://twitter.com/DavidNind)
* Phone: +64 21 0537 847

# Tools used to create this site

* [Git] for version control
* [GitLab] for Git repository management
* [GitLab Pages] for site hosting
* [Jekyll] static site generator
* [Let's Encrypt] for free certificates

You can find the source code for this site and its content at [GitLab](https://gitlab.com/davidnind/davidnind.gitlab.io/).

[Git]: https://git-scm.com/
[GitLab]: https://gitlab.com/
[GitLab Pages]: https://pages.gitlab.io/
[Jekyll]: https://jekyllrb.com/
[Let's Encrypt]: https://letsencrypt.org/
